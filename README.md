# dmalloc

Tiny debug versions of the C standard library memory allocation functions, which checks for underflow and overflow.

## Usage

Import `dmalloc.h` and `dmalloc.c` into your project and use `dmalloc`/`drealloc`/`dfree` for memory allocation instead of `malloc`/`realloc`/`free`. Their interfaces are identical to their standard library counterparts, including pre- and postconditions.

If the `DMALLOC` macro is set, then the debug functionality is activated. Otherwise `dmalloc`/`drealloc`/`dfree` simply maps directly onto the standard library counterparts. `DMALLOC` is set if various debug macros are set, or it can be set manually. If `DMALLOC` is not set in any translation unit, then `dmalloc.c` is superfluous.

Never mix the usage of `dmalloc`/`drealloc`/`dfree` with usage of `malloc`/`realloc`/`free`.