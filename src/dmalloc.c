/*
 * Copyright (C) 2021 by Claus Jensby Madsen <clausjensbymadsen@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <assert.h>
#include <string.h>
#include <stdio.h>

#include "dmalloc.h"

size_t memory_allocation_count = 0;
size_t memory_allocation_amount = 0;

/* dmalloc allocates more memory than what the caller requests. The
 * additional memory is used for debug information like the siza of the
 * allocation as well as padding the allocation for underflow/overflow
 * detection.
 *
 *                  /--- size ---\
 * +------+---------+------------+---------+
 * | size | padding | allocation | padding |
 * +------+---------+------------+---------+
 *                  ^
 *                  dmalloc returns this address to caller
 */

#define DMALLOC_PADDING_SIZE sizeof(size_t)
#define DMALLOC_PADDING_VALUE 0x33

// Don't call debug_malloc directly. Use macro dmalloc in dmalloc.h.
void* debug_malloc(size_t size)
{
  assert(size > 0);
  
  void* alloc = malloc(size + sizeof(size_t) + 2*DMALLOC_PADDING_SIZE);
  if (alloc == NULL) return NULL;

  memory_allocation_count++;
  memory_allocation_amount += size;

  *(size_t*)alloc = size;
  memset((char*)alloc + sizeof(size_t),
         DMALLOC_PADDING_VALUE,
         DMALLOC_PADDING_SIZE);
  memset((char*)alloc + sizeof(size_t) + DMALLOC_PADDING_SIZE + size,
         DMALLOC_PADDING_VALUE,
         DMALLOC_PADDING_SIZE);

  return (char*)alloc + sizeof(size_t) + DMALLOC_PADDING_SIZE;
}

// TODO: Add debug_calloc function.

// Don't call debug_realloc directly. Use macro drealloc in dmalloc.h.
void* debug_realloc(void* ptr, size_t size)
{
  assert(size > 0);

  size_t old_size = *(size_t*)((char*)ptr - DMALLOC_PADDING_SIZE - sizeof(size_t));

  void* alloc = realloc((char*)ptr - sizeof(size_t) - 8, size + sizeof(size) + 2*DMALLOC_PADDING_SIZE);
  if (alloc == NULL) return NULL;

  *(size_t*)alloc = size;
  memset((char*)alloc + sizeof(size),
         DMALLOC_PADDING_VALUE,
         DMALLOC_PADDING_SIZE);
  memset((char*)alloc + sizeof(size) + DMALLOC_PADDING_SIZE + size,
         DMALLOC_PADDING_VALUE,
         DMALLOC_PADDING_SIZE);

  memory_allocation_amount -= old_size;
  memory_allocation_amount += size;

  return (char*)alloc + sizeof(size) + DMALLOC_PADDING_SIZE;
}

// Don't call debug_free directly. Use macro dfree in dmalloc.h.
void debug_free(void* ptr)
{
  if (ptr == NULL) return;

  size_t size = *(size_t*)((char*)ptr - DMALLOC_PADDING_SIZE - sizeof(size_t));

  // Check if the tally is sound
  if (memory_allocation_count == 0)
  {
    fprintf(stderr, "dfree called more times than dmalloc!\n");
    return;
  }
  memory_allocation_count--;

  if (memory_allocation_amount < size)
  {
    fprintf(stderr, "Freeing more memory than allocated!\n");
    return;
  }
  memory_allocation_amount -= size;

  // Check if the padding is intact
  char* p = ptr;
  for(int i = 0; i < DMALLOC_PADDING_SIZE; ++i)
  {
    p--;
    if (*p != DMALLOC_PADDING_VALUE)
    {
      fprintf(stderr, "Memory underflow error! "
                      "(malloc address: %p, dmalloc address: %p)\n",
                      ptr, (char*)ptr+sizeof(size_t)+DMALLOC_PADDING_SIZE);
      abort();
    }
  }
  p = ptr + size;
  for(int i = 0; i < DMALLOC_PADDING_SIZE; ++i)
  {
    if (*p != DMALLOC_PADDING_VALUE)
    {
      fprintf(stderr, "Memory overflow error! "
                      "(malloc address: %p, dmalloc address: %p)\n",
                      ptr, (char*)ptr+sizeof(size_t)+DMALLOC_PADDING_SIZE);
      abort();
    }
    p++;
  }

  free((char*)ptr - DMALLOC_PADDING_SIZE - sizeof(size_t));
}
