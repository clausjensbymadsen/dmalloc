
/*
 * Copyright (C) 2021 by Claus Jensby Madsen <clausjensbymadsen@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef DMALLOC_H
#define DMALLOC_H

#include <stdlib.h>

#if !defined(DMALLOC)
  #if defined(DEBUG) || defined(_DEBUG)
    #define DMALLOC
  #endif
#endif

#if defined(DMALLOC)
  #define dmalloc(size) debug_malloc(size)
  #define drealloc(ptr, size) debug_realloc(ptr, size)
  #define dfree(ptr) debug_free(ptr)
#else
  #define dmalloc(size) malloc(size)
  #define drealloc(ptr, size) realloc(ptr, size)
  #define dfree(ptr) free(ptr)
#endif

#endif
